#
# Be sure to run `pod lib lint LocationServices.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LocationServices'
  s.version          = '1.0.3'
  s.summary          = 'It will genarate the userlocation information and share that information to the backend.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'This is sai chandra. I am an IOS Developer and coding in swift and doing this location services pod specs and implementing the location services related changes in this project.'

  s.homepage         = 'https://gitlab.com/saichandra1992/LocationServices'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'saichandra' => 'saidevabhakthuni23@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/saichandra1992/LocationServices.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  s.swift_versions = '5.0'

  s.source_files = 'LocationServices/Classes/**/*'
  
  # s.resource_bundles = {
  #   'LocationServices' => ['LocationServices/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
